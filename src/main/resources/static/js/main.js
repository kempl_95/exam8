'use strict';
const baseUrl = "https://restcountries.eu/rest/v2";
const searchForm = document.getElementById('search_form');
const answerTeg = document.getElementById('answer');
let countriesArray = [];
searchForm.addEventListener('submit', onSearchHandler);

function onSearchHandler(e) {
    e.preventDefault();
    const form = e.target;
    const dataForm = new FormData(form);
    const data =  Object.fromEntries(dataForm);
    getCountry(data).then(r => null).catch(e => console.log("Ошибка: " + e));
}

async function getCountry(data) {
    countriesArray = [];
    const response = await fetch(baseUrl + '/name/' + data.text + '?fields=name;capital;flag;currencies;regionalBlocs');
    const responseData = await response.json();
    if (response.status === 404)
        notFound();
    else {
        for (let i=0; i<responseData.length; i++){
            countriesArray.push(new Country(responseData[i]));
        }
        for (let i=0; i<countriesArray.length;i++){
            addCountry(createCountryElement(countriesArray[i]));
        }
        document.getElementById("search_text").value = '';
    }
}

function notFound() {
    alert("Страна с указанным именем не найдена! Попробуйте снова");
    document.getElementById("search_text").value = '';
}

function Country(data){
    this.name = data.name;
    this.capital = data.capital;
    this.flag = data.flag;
    for (let i = 0; i<data.currencies.length; i++){
        this.currencies = {
            code: data.currencies[i].code,
            name: data.currencies[i].name
        };
    }
    for (let i = 0; i<data.regionalBlocs.length; i++){
        this.regionalBlocs = {
            name: data.regionalBlocs[i].name
        };
    }
}
function createCountryElement(country) {
    let countryElement = document.createElement('div');
    countryElement.className = "country mb-3";
    countryElement.innerHTML =
        `<h4 class="d-block mb-2">${country.name}</h4>`+
        `<span class="d-block mb-2">Capital: ${country.capital}</span>`+
        `<img class="d-block w-100 mb-2" src="${country.flag}">`+
        `<span class="d-block">Currency: ${country.currencies.name}</span>`+
        `<span class="d-block">Region: ${country.regionalBlocs.name}</span>`+
        `<a class="d-block" href="https://www.google.com/search?q=${country.name}"  target="_blank">Google</a>`+
        `<a class="d-block" href="https://ru.m.wikipedia.org/wiki/${country.name}"  target="_blank">Wiki</a>`+
        '<hr>'
    ;
    return countryElement;
}

function addCountry(countryElement) {
    answerTeg.append(countryElement);
}

