package kg.exam8.countrysearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountrysearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(CountrysearchApplication.class, args);
    }

}
